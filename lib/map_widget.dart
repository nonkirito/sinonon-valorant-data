import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MapWidget extends StatefulWidget {
  MapWidget({Key? key}) : super(key: key);

  @override
  _Map_WidgetState createState() => _Map_WidgetState();
}

class _Map_WidgetState extends State<MapWidget> {
  final Stream<QuerySnapshot> _mapStream =
      FirebaseFirestore.instance.collection('maps').orderBy('name').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Maps'),
      ),
      body: StreamBuilder(
        stream: _mapStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return Container(
                padding: EdgeInsets.only(top: 10, left: 30, right: 30),
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Card(
                        color: Colors.amberAccent,
                        child: InkWell(
                          onTap: () {
                            print("tapped");
                          },
                          child: Container(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    data['image'],
                                    width: 250,
                                    height: 250,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(data['name']),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                            width: 300.0,
                            height: 300.0,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}

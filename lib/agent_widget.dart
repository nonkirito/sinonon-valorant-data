import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sinonon_valorant_data/agent_detail.dart';


class AgentWidget extends StatefulWidget {
  AgentWidget({Key? key}) : super(key: key);

  @override
  _AgentWidgetState createState() => _AgentWidgetState();
}

class _AgentWidgetState extends State<AgentWidget> {
  final Stream<QuerySnapshot> _agentStream = FirebaseFirestore.instance
      .collection('agents')
      .orderBy('name')
      .snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Agents'),
      ),
      body: StreamBuilder(
        stream: _agentStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return Container(
                padding: EdgeInsets.only(top: 10, left: 30, right: 30),
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ListTile(
                        leading: Image.asset(data['image']),
                        title: Text(data['name']),
                        subtitle: Text(data['role']),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            child: Text('View'),
                            onPressed: () {
                              Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AgentDetail(agentData: data, skillList: data['skills'],)));
                            },
                          )
                        ],
                      )
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:sinonon_valorant_data/about_widget.dart';
import 'package:sinonon_valorant_data/player_crosshair_widget.dart';
import 'package:sinonon_valorant_data/rank_widget.dart';
import 'package:sinonon_valorant_data/weapon_widget.dart';
import 'agent_widget.dart';
import 'map_widget.dart';

class MenuButtonWidget extends StatelessWidget {
  const MenuButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Container(
          child: Image.asset('images/valorant wallpaper.jpg',
              height: 400, width: 750),
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("Agents"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AgentWidget()))
              },
              splashColor: Colors.redAccent,
            ),
            SizedBox(
              width: 5,
            ),
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("Maps"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MapWidget()))
              },
              splashColor: Colors.redAccent,
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("Weapons"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WeaponWidget()))
              },
              splashColor: Colors.redAccent,
            ),
            SizedBox(
              width: 5,
            ),
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("Player Crosshair"),
              onPressed: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PlayerCrosshairWidget()))
              },
              splashColor: Colors.redAccent,
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("Rank"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RankWidget()))
              },
              splashColor: Colors.redAccent,
            ),
            SizedBox(
              width: 5,
            ),
            MaterialButton(
              height: 90.0,
              minWidth: 360.0,
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              child: Text("About"),
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutWidget()))
              },
              splashColor: Colors.redAccent,
            )
          ],
        ),
      ],
    );
  }
}

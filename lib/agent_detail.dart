import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sinonon_valorant_data/video_widget.dart';

class AgentDetail extends StatefulWidget {
  AgentDetail({Key? key, required this.agentData, required this.skillList})
      : super(key: key);
  final Map<String, dynamic> agentData;
  final List<dynamic> skillList;
  @override
  _AgentDetailState createState() =>
      _AgentDetailState(this.agentData, this.skillList);
}

class _AgentDetailState extends State<AgentDetail> {
  final Map<String, dynamic> agentData;
  final List<dynamic> skillList;
  _AgentDetailState(this.agentData, this.skillList);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(agentData['name']),
        ),
        body: Center(
          child: Column(
            children: [
              Card(
                color: Colors.amberAccent,
                child: InkWell(
                  onTap: () {
                    print("tapped");
                  },
                  child: Container(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            agentData['image'],
                            width: 110,
                            height: 110,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(agentData['name']),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                agentData['imgRole'],
                                width: 20,
                                height: 20,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(agentData['role'])
                            ],
                          )
                        ],
                      ),
                    ),
                    width: 210.0,
                    height: 210.0,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Column(
                  children: [
                    Card(
                        color: Colors.amberAccent,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Image.asset(skillList[0]['image']),
                                title: Text(skillList[0]['name']),
                                subtitle: Text('C'),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    TextButton(
                                        child: const Text('View'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      VideoPlayerScreen(
                                                        skillList: skillList[0],
                                                      )));
                                        })
                                  ])
                            ])),
                    Card(
                        color: Colors.amberAccent,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Image.asset(skillList[1]['image']),
                                title: Text(skillList[1]['name']),
                                subtitle: Text('Q'),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    TextButton(
                                        child: const Text('View'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      VideoPlayerScreen(
                                                        skillList: skillList[1],
                                                      )));
                                        })
                                  ])
                            ])),
                    Card(
                        color: Colors.amberAccent,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Image.asset(skillList[2]['image']),
                                title: Text(skillList[2]['name']),
                                subtitle: Text('E'),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    TextButton(
                                        child: const Text('View'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      VideoPlayerScreen(
                                                        skillList: skillList[2],
                                                      )));
                                        })
                                  ])
                            ])),
                    Card(
                        color: Colors.amberAccent,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ListTile(
                                leading: Image.asset(skillList[3]['image']),
                                title: Text(skillList[3]['name']),
                                subtitle: Text('X'),
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    TextButton(
                                        child: const Text('View'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      VideoPlayerScreen(
                                                        skillList: skillList[3],
                                                      )));
                                        })
                                  ])
                            ]))
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

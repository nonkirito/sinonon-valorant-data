import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class PlayerCrosshairWidget extends StatefulWidget {
  PlayerCrosshairWidget({Key? key}) : super(key: key);

  @override
  _PlayerCrosshairWidgetState createState() => _PlayerCrosshairWidgetState();
}

class _PlayerCrosshairWidgetState extends State<PlayerCrosshairWidget> {
  final Stream<QuerySnapshot> _agentStream = FirebaseFirestore.instance
      .collection('players')
      .orderBy('name')
      .snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Player Crosshair'),
      ),
      body: StreamBuilder(
        stream: _agentStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return Container(
                padding: EdgeInsets.only(top: 10, left: 30, right: 30),
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ListTile(
                        leading: Image.asset(data['image'], width: 75, height: 75,),
                        title: Text(data['name']),
                        subtitle: Text('Crosshair: ' + data['crosshair']),
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
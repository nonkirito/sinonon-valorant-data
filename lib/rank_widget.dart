import 'package:flutter/material.dart';

class RankWidget extends StatefulWidget {
  RankWidget({Key? key}) : super(key: key);

  @override
  _RankWidgetState createState() => _RankWidgetState();
}

class _RankWidgetState extends State<RankWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rank'),
      ),
      body: Center(
        child: Column(children: [
          SizedBox(
            height: 30,
          ),
          Container(
            child: Image.asset('images/ranks.png', height: 400, width: 750),
          ),
        ]),
      ),
    );
  }
}

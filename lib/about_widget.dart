import 'package:flutter/material.dart';

class AboutWidget extends StatefulWidget {
  AboutWidget({Key? key}) : super(key: key);

  @override
  _AboutWidgetState createState() => _AboutWidgetState();
}

class _AboutWidgetState extends State<AboutWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
      body: Container(
        child: Center(
            child: Column(
              children: [
                SizedBox(height: 30,),
                Text('About This App', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
                SizedBox(height: 30,),
                Text(
          '''
    This app will give you information. News and knowledge within the Valorant game whether
- Agents, various skills information of each Agent and playing techniques
- Maps, information of each map that has a call point or callout of each map and a spike point of each site
- Weapons Information about the guns used in each gun play. and the price of the gun
- Pro Player Crosshair Aims used by each pro player.
- Some other information about the game''',
          maxLines: 20,
          style: TextStyle(
                  fontSize: 16.0, color: Colors.black),
        ),
              ],
            )),
      ),
    );
  }
}

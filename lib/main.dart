import 'package:flutter/material.dart';
import 'package:sinonon_valorant_data/menu_button_widget.dart';
import 'package:sinonon_valorant_data/nav_drawer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sinonon Valorant Data',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Sinonon Valorant Data'),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: MenuButtonWidget(),
        ),
        drawer: NavDrawer(),
      ),
    );
  }
}

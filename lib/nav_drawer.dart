import 'package:flutter/material.dart';

import 'about_widget.dart';
import 'agent_widget.dart';
import 'map_widget.dart';
import 'player_crosshair_widget.dart';
import 'rank_widget.dart';
import 'weapon_widget.dart';

class NavDrawer extends StatefulWidget {
  NavDrawer({Key? key}) : super(key: key);

  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  bool isSwitch = false;
  var bgColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              child: Text(
                'Menu',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              decoration: BoxDecoration(
                color: Colors.green,
                 image: DecorationImage(
                     fit: BoxFit.fill,
                     image: AssetImage('images/valorant wallpaper.jpg')
              )),
           ),
          ListTile(
            title: Text('Agents'),
            onTap: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AgentWidget()))
            },
          ),
          ListTile(
            title: Text('Maps'),
            onTap: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MapWidget()))
            },
          ),
          ListTile(
            title: Text('Weapons'),
            onTap: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => WeaponWidget()))
            },
          ),
          ListTile(
            title: Text('Player Crosshair'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PlayerCrosshairWidget()))
            },
          ),
          ListTile(
            title: Text('Rank'),
            onTap: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RankWidget()))
            },
          ),
          ListTile(
            title: Text('About'),
            onTap: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AboutWidget()))
            },
          ),
          ListTile(
              title: Text('Dark Mode'),
              trailing: Switch(
                value: isSwitch,
                onChanged: (value) {
                  setState(() {
                    isSwitch = value;
                    print(isSwitch);
                  });
                },
                activeTrackColor: Colors.lightGreenAccent,
                activeColor: Colors.green,
              )),
        ],
      ),
    );
  }
}
